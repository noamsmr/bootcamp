import React from 'react';
import Cell from './Cell';
import '../style.css';

const Board = ({ boardState, cellOnClick }) => {
  return (
    <div className="boardContainer">
      <Cell
        row={0}
        col={0}
        cellState={boardState[0][0]}
        cellOnClick={cellOnClick}
      />
      <Cell
        row={0}
        col={1}
        cellState={boardState[0][1]}
        cellOnClick={cellOnClick}
      />
      <Cell
        row={0}
        col={2}
        cellState={boardState[0][2]}
        cellOnClick={cellOnClick}
      />

      <Cell
        row={1}
        col={0}
        cellState={boardState[1][0]}
        cellOnClick={cellOnClick}
      />
      <Cell
        row={1}
        col={1}
        cellState={boardState[1][1]}
        cellOnClick={cellOnClick}
      />
      <Cell
        row={1}
        col={2}
        cellState={boardState[1][2]}
        cellOnClick={cellOnClick}
      />

      <Cell
        row={2}
        col={0}
        cellState={boardState[2][0]}
        cellOnClick={cellOnClick}
      />
      <Cell
        row={2}
        col={1}
        cellState={boardState[2][1]}
        cellOnClick={cellOnClick}
      />
      <Cell
        row={2}
        col={2}
        cellState={boardState[2][2]}
        cellOnClick={cellOnClick}
      />
    </div>
  );
};

export default Board;
