import React from 'react';
import '../style.css';

const Cell = ({ row, col, cellState, cellOnClick }) => {
  var classString = 'cell';
  if (cellState.cellFlash) {
    classString = 'cell cellFlash';
  }

  return (
    <span className={classString} onClick={() => cellOnClick(row, col)}>
      {cellState.cellFill}
    </span>
  );
};

export default Cell;
