import React from 'react';

const Player = ({ playerInfo }) => {
  const name = `${playerInfo.name.title} ${playerInfo.name.first} ${
    playerInfo.name.last
  }`;
  return <div>{name}</div>;
};

export default Player;
