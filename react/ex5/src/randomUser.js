const randomUser = callback => {
  var request = new XMLHttpRequest();
  request.open('GET', 'https://randomuser.me/api/?results=2', true);

  request.onload = function() {
    let data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
      callback(data.results[0], data.results[1]);
    } else {
      console.log('error');
    }
  };
  request.send();
};

const randomUser1 = async callback => {
  const response = await fetch('https://randomuser.me/api/?results=2');
  const json = await response.json();
  const data = json.results;

  // const response = fetch('https://randomuser.me/api/?results=2')
  //   .then(res => res.json())
  //   .then(data => {
  //     console.log(data);
  //     callback(data.results[0], data.results[1]);
  //   })
  //   .catch(err => console.log(err));
};

export default randomUser;
