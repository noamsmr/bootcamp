import React from 'react';
import { connect } from 'react-redux';
import Board from './Components/Board';
import * as gameActions from './redux/actions/gameActions';
import Player from './Components/Player';
import randomUser from './randomUser';

const NEARBY_CELL_DIRECTIONS = {
  aboveLeft: { y: -1, x: -1 },
  above: { y: -1, x: 0 },
  aboveRight: { y: -1, x: 1 },
  right: { y: 0, x: 1 },
  belowRight: { y: 1, x: 1 },
  below: { y: 1, x: 0 },
  belowLeft: { y: 1, x: -1 },
  left: { y: 0, x: -1 }
};

class Game extends React.Component {
  massageClassToggle = '';

  isCellAvilable = (row, col) => {
    return this.props.boardState[row][col].cellFill === '';
  };

  insertSign = (row, col) => {
    let isInserted = false;
    if (this.isCellAvilable(row, col)) {
      if (this.props.correntPlayer === 'X') {
        this.props.addX(row, col);
        this.props.increaseTurnCounter();
        isInserted = true;
      } else if (this.props.correntPlayer === 'O') {
        this.props.addO(row, col);
        this.props.increaseTurnCounter();
        isInserted = true;
      }
    }
    return isInserted;
  };

  switchCorrentPlayer = () => {
    if (this.props.correntPlayer === 'X') {
      this.props.switchCorrentPlayerTo('O');
    } else this.props.switchCorrentPlayerTo('X');
  };

  isWithinBoard(row, col) {
    return row >= 0 && row < 3 && col >= 0 && col < 3;
  }

  isEmptyCell(row, col) {
    return (
      this.isWithinBoard(row, col) &&
      this.props.boardState[row][col].cellFill === ''
    );
  }

  isCellEqualNearbyCell(row, col, direction) {
    if (
      this.isWithinBoard(row + direction.y, col + direction.x) &&
      this.props.boardState[row + direction.y][col + direction.x].cellFill ===
        this.props.correntPlayer
    ) {
      return true;
    } else {
      return false;
    }
  }

  switchReverseDirection(direction) {
    return { y: direction.y * -1, x: direction.x * -1 };
  }

  getSequenceStart(row, col, direction) {
    let reverseDirection = this.switchReverseDirection(direction);
    while (this.isCellEqualNearbyCell(row, col, reverseDirection)) {
      row += reverseDirection.y;
      col += reverseDirection.x;
    }
    return { row, col };
  }

  isSequenceFromStart(row, col, direction) {
    let counter = 1;
    while (this.isCellEqualNearbyCell(row, col, direction)) {
      row += direction.y;
      col += direction.x;
      counter++;
    }
    return counter >= 3;
  }

  isAnySequence(row, col) {
    let directions = [
      NEARBY_CELL_DIRECTIONS.belowRight,
      NEARBY_CELL_DIRECTIONS.belowLeft,
      NEARBY_CELL_DIRECTIONS.below,
      NEARBY_CELL_DIRECTIONS.right
    ];
    for (let index = 0; index < directions.length; index++) {
      let axis = this.getSequenceStart(row, col, directions[index]);
      if (this.isSequenceFromStart(axis.row, axis.col, directions[index])) {
        this.markSequence(axis.row, axis.col, directions[index]);
        this.props.addEndMassage(this.props.correntPlayer + ' won.');
        return true;
      }
    }
    return false;
  }

  markSequence = (row, col, direction) => {
    while (this.isCellEqualNearbyCell(row, col, direction)) {
      this.props.boardState[row][col].cellFlash = true;
      row += direction.y;
      col += direction.x;
    }
    this.props.boardState[row][col].cellFlash = true;
  };

  isTie = () => {
    if (this.props.turnCounter === 8) {
      this.props.addEndMassage("It's a tie.");
    }
  };

  playMove = (row, col) => {
    if (!this.props.gameStatus) {
      return;
    }

    if (this.insertSign(row, col)) {
      this.switchCorrentPlayer();
    }

    if (this.isAnySequence(row, col) || this.isTie())
      this.props.toggleGameStatus();
  };

  componentDidMount() {
    randomUser((player1, player2) => this.props.addPlayers(player1, player2));
  }

  render() {
    return (
      <>
        <Board boardState={this.props.boardState} cellOnClick={this.playMove} />
        {this.props.endMassage && (
          <div className="endMassage" onClick={this.props.restartGame}>
            {this.props.endMassage} click to restart.
          </div>
        )}
        {this.props.players.X && <Player playerInfo={this.props.players.X} />}
        {this.props.players.O && <Player playerInfo={this.props.players.O} />}
      </>
    );
  }
}

const mapStateToProps = state => ({
  boardState: state.boardState,
  correntPlayer: state.correntPlayer,
  players: state.players,
  turnCounter: state.turnCounter,
  gameStatus: state.gameStatus,
  endMassage: state.endMassage
});

const mapDispatchToProps = dispatch => ({
  addX: (row, col) => dispatch(gameActions.addX(row, col)),
  addO: (row, col) => dispatch(gameActions.addO(row, col)),
  increaseTurnCounter: () => dispatch(gameActions.increaseTurnCounter()),
  flashCell: (row, col) => dispatch(gameActions.flashCell(row, col)),
  switchCorrentPlayerTo: player =>
    dispatch(gameActions.switchCorrentPlayerTo(player)),
  addPlayers: (playerX, playerO) =>
    dispatch(gameActions.addPlayers(playerX, playerO)),
  toggleGameStatus: () => dispatch(gameActions.toggleGameStatus()),
  addEndMassage: massage => dispatch(gameActions.addEndMassage(massage)),
  restartGame: () => dispatch(gameActions.restartGame())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);
