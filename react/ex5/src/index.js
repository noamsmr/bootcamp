import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import gameReducer from './redux/reducers/gameReducer';
import Game from './Game';

const store = createStore(gameReducer);

render(
  <Provider store={store}>
    <Game />
  </Provider>,
  document.getElementById('root')
);
