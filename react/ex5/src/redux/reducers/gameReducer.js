export default function matrixReducer(
  state = {
    boardState: [
      [
        { cellFill: '', cellFlash: false },
        { cellFill: '', cellFlash: false },
        { cellFill: '', cellFlash: false }
      ],
      [
        { cellFill: '', cellFlash: false },
        { cellFill: '', cellFlash: false },
        { cellFill: '', cellFlash: false }
      ],
      [
        { cellFill: '', cellFlash: false },
        { cellFill: '', cellFlash: false },
        { cellFill: '', cellFlash: false }
      ]
    ],
    correntPlayer: 'X',
    players: {},
    turnCounter: 0,
    endMassage: '',
    gameStatus: 1
  },
  action
) {
  let newState = [...state.boardState];

  switch (action.type) {
    case 'ADD_X':
      newState[action.row][action.col].cellFill = 'X';
      return Object.assign({}, state, { boardState: newState });

    case 'ADD_O':
      newState[action.row][action.col].cellFill = 'O';
      return Object.assign({}, state, { boardState: newState });

    case 'INCREASE_TURN_COUNTER':
      let turn = state.turnCounter + 1;
      return Object.assign({}, state, { turnCounter: turn });

    case 'FLASH_CELL':
      newState[action.row][action.col].cellFlash = true;
      return Object.assign({}, state, { boardState: newState });

    case 'SWITCH_CORRENT_PLAYER_TO':
      return Object.assign({}, state, { correntPlayer: action.player });

    case 'ADD_PLAYERS':
      return Object.assign({}, state, {
        players: { X: action.playerX, O: action.playerO }
      });

    case 'TOGGLE_GAME_STATUS':
      return Object.assign({}, state, {
        gameStatus: (state.gameStatus + 1) % 2
      });

    case 'ADD_END_MASSAGE':
      return Object.assign({}, state, { endMassage: action.massage });

    case 'RESTART_GAME':
      return {
        boardState: getEmptyBoard(),
        correntPlayer: 'X',
        players: {},
        turnCounter: 0,
        endMassage: null,
        gameStatus: 1
      };

    default:
      return state;
  }
}

const getEmptyBoard = () => {
  return [
    [
      { cellFill: '', cellFlash: false },
      { cellFill: '', cellFlash: false },
      { cellFill: '', cellFlash: false }
    ],
    [
      { cellFill: '', cellFlash: false },
      { cellFill: '', cellFlash: false },
      { cellFill: '', cellFlash: false }
    ],
    [
      { cellFill: '', cellFlash: false },
      { cellFill: '', cellFlash: false },
      { cellFill: '', cellFlash: false }
    ]
  ];
};
