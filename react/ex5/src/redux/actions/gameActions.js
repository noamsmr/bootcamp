import { ADD_X } from '../action-types/actionTypes';

export function addX(row, col) {
  return { type: ADD_X, row, col };
}

export function addO(row, col) {
  return { type: 'ADD_O', row, col };
}

export function increaseTurnCounter() {
  return { type: 'INCREASE_TURN_COUNTER' };
}

export function flashCell(row, col) {
  return { type: 'FLASH_CELL', row, col };
}

export function switchCorrentPlayerTo(player) {
  return { type: 'SWITCH_CORRENT_PLAYER_TO', player };
}

export function addPlayers(playerX, playerO) {
  return { type: 'ADD_PLAYERS', playerX, playerO };
}

export function toggleGameStatus() {
  return { type: 'TOGGLE_GAME_STATUS' };
}

export function addEndMassage(massage) {
  return { type: 'ADD_END_MASSAGE', massage };
}

export function restartGame() {
  return { type: 'RESTART_GAME' };
}
