import React from 'react';
import ProtoTypes from 'prop-types';
import Row from './Row';

const Matrix = ({ length, row, col }) => {
  const bindRows = numOfRow => {
    let elements = [];
    for (let i = 1; i < numOfRow + 1; i++) {
      if (row === i) {
        elements.push(<Row key={i} numOfCol={numOfRow} col={col} />);
      } else elements.push(<Row key={i} numOfCol={numOfRow} />);
    }
    return elements;
  };

  return <div className="container">{bindRows(length)}</div>;
};

Matrix.prototype = {
  length: ProtoTypes.number.isRequired
};

export default Matrix;
