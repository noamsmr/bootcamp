import React from 'react';
import ProtoType from 'prop-types';
import '../style.css';

const Selector = ({ typeFunc, numOfOptions, selectValue }) => {
  const bindOptions = numberOfOptions => {
    let elements = [];
    for (let i = 1; i < numberOfOptions + 1; i++) {
      elements.push(
        <option key={i} value={i}>
          {i}
        </option>
      );
    }
    return elements;
  };
  return (
    <>
      <div className="clear">
        <select onChange={typeFunc} value={selectValue}>
          {bindOptions(numOfOptions)}
        </select>
      </div>
    </>
  );
};

export default Selector;

Selector.prototype = {
  numberOfOptions: ProtoType.number.isRequeird,
  typeFunc: ProtoType.func.isRequeird,
  selectValue: ProtoType.number
};
