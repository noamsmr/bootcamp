import React from 'react';
import Matrix from './Matrix';
import Selector from './Selector';
import { connect } from 'react-redux';
import * as matrixActions from '../redux/actions/matrixActions';
import '../style.css';

class HomePage extends React.Component {
  handleRowChange = event => this.props.setRow(parseInt(event.target.value));
  handleColChange = event => this.props.setCol(parseInt(event.target.value));
  handleLengthChange = event =>
    this.props.setMatrixLength(parseInt(event.target.value));

  render() {
    return (
      <>
        <Matrix
          length={this.props.length}
          row={this.props.row}
          col={this.props.col}
        />
        <div className="clear">Row:</div>
        <Selector
          typeFunc={this.handleRowChange}
          numOfOptions={this.props.length}
          selectValue={this.props.row}
        />
        Col:
        <Selector
          typeFunc={this.handleColChange}
          numOfOptions={this.props.length}
          selectValue={this.props.col}
        />
        Length:
        <Selector
          typeFunc={this.handleLengthChange}
          numOfOptions={10}
          selectValue={this.props.length}
        />
      </>
    );
  }
}

const mapStateToProps = state => ({
  row: state.matrix.row,
  col: state.matrix.col,
  length: state.matrix.matrixLength
});

const mapDispatchToProps = dispatch => ({
  setRow: row => dispatch(matrixActions.setRow(row)),
  setCol: col => dispatch(matrixActions.setCol(col)),
  setMatrixLength: length => dispatch(matrixActions.setMatrixLength(length))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
