import React from 'react';
import ProtoType from 'prop-types';
import '../style.css';

const Row = ({ numOfCol, col }) => {
  const bindCells = numOfCol => {
    let elements = [];
    for (let i = 1; i < numOfCol + 1; i++) {
      if (col === i)
        elements.push(
          <div className="cell selectedCell" key={i}>
            {i}
          </div>
        );
      else
        elements.push(
          <div className="cell" key={i}>
            {i}
          </div>
        );
    }
    return elements;
  };

  return (
    <>
      <div className="clear">{bindCells(numOfCol)}</div>
    </>
  );
};

export default Row;

Row.prototype = {
  numOfCol: ProtoType.number.isRequeird,
  col: ProtoType.number
};
