export default function matrixReducer(
  state = {
    row: 1,
    col: 1,
    matrixLength: 3
  },
  action
) {
  switch (action.type) {
    case 'ROW':
      return Object.assign({}, state, { row: action.rowNum });
    case 'COL':
      return Object.assign({}, state, { col: action.colNum });
    case 'MATRIX_LENGTH':
      return Object.assign({}, state, { matrixLength: action.matrixLength });
    default:
      return state;
  }
}
