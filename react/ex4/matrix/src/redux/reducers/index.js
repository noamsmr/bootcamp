import { combineReducers } from 'redux';
import matrix from './matrixReducer';

const rootReducer = combineReducers({ matrix });

export default rootReducer;
