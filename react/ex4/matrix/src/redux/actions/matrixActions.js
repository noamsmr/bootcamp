export function setRow(rowNum) {
  return { type: 'ROW', rowNum };
}

export function setCol(colNum) {
  return { type: 'COL', colNum };
}

export function setMatrixLength(matrixLength) {
  return { type: 'MATRIX_LENGTH', matrixLength };
}
