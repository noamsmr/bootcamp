import React from 'react';
import PropTypes from 'prop-types';

const ReuseableComponent = ({ string, func, times }) => {
  const bindElements = times => {
    let elements = [];
    for (let i = 0; i < times; i++) {
      elements.push(<div key={i}>{string}</div>);
    }
    return elements;
  };

  return <div onClick={() => func(string)}>{bindElements(times)}</div>;
};

ReuseableComponent.propTypes = {
  string: PropTypes.string.isRequired,
  func: PropTypes.func.isRequired,
  times: PropTypes.number.isRequired
};

export default ReuseableComponent;
