import React from 'react';
import ReuseableComponent from './components/ReuseableComponent';

class App extends React.Component {
  logFunction = string => console.log(string);

  render() {
    return (
      <>
        <ReuseableComponent
          string="string1"
          func={this.logFunction}
          times={1}
        />
        <ReuseableComponent
          string="string2"
          func={this.logFunction}
          times={2}
        />
        <ReuseableComponent
          string="string3"
          func={this.logFunction}
          times={4}
        />
      </>
    );
  }
}

export default App;
