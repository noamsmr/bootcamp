import React from 'react';

const Header = ({ counter }) => <h2>{counter}</h2>;

export default Header;
