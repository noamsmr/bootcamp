import React from 'react';
import Header from './Header';
import { connect } from 'react-redux';
import * as counterActions from '../redux/actions/CounterActions';
import PropTypes from 'prop-types';

class HomePage extends React.Component {
  render() {
    return (
      <>
        <Header counter={this.props.counter} />
        <button onClick={this.props.decrease}>-1</button>
        <button onClick={this.props.increase}>+1</button>
      </>
    );
  }
}

HomePage.propTypes = {
  dispatch: PropTypes.func
};

const mapStateToProps = state => ({ counter: state.counter });

const mapDispatchToProps = dispatch => ({
  increase: () => dispatch(counterActions.increaseCounter()),
  decrease: () => dispatch(counterActions.decreaseCounter())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
