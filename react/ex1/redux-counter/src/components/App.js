import React from 'react';
import HomePage from './HomePage';
import AnotherPage from './AnotherPage';
import NavBar from './NavBar';
import { Route } from 'react-router-dom';

const App = () => (
  <>
    <NavBar />
    <Route path="/" exact component={HomePage} />
    <Route path="/AnotherPage" component={AnotherPage} />
  </>
);

export default App;
