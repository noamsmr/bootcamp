import React from 'react';
import Header from './Header';

class HomePage extends React.Component {
  state = {
    counter: 0
  };

  increaseCounter = () => {
    this.setState(prevState => ({ counter: prevState.counter + 1 }));
  };

  decreaseCounter = () => {
    this.setState(prevState => ({ counter: prevState.counter - 1 }));
  };

  render() {
    return (
      <>
        <Header counter={this.state.counter} />
        <button onClick={this.decreaseCounter}>-1</button>
        <button onClick={this.increaseCounter}>+1</button>
      </>
    );
  }
}

export default HomePage;
