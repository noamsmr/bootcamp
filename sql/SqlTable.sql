CREATE TABLE "Employee" ( 
	"Id" INT,
	"First name" VARCHAR( 20 ),
	"Last name" VARCHAR( 20 ),
	"Age" INT,
	"Address" VARCHAR( 60 ),
	"Creation date" Date,
	PRIMARY KEY ( "Id" ),
	CHECK AGE BETWEEN 0 AND 120
 );